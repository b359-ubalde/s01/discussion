# This is a Comment
#  Comments in Python are done using the (#) or hashtag symbol
# Comments are done by clicking CTRL + / = One Line Comment

"""
Although we have no keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""

# Python Syntax
# Print "Hello World" in Python
print("Hello World!")

# Indentation
# In Python, indentation is used to indicate a block of our code.
# Where in other languages, the indentation in the code is for readability. Indentation in Python is very important.


# Variables
# You create a varaible by typing the variableName and its content
# No need to state its data types
# Variables in Python are called Identifier
# Python uses SNAKE CASE
# All Identifiers should begin with a letter (A - Z), dollar sign ($) or un underscore
age = 18
middle_initial = "C"

# You can assign multiple values in multipleidentifiers in one line
name1, name2, name3, name4 = "John", "Edwin", "Kenneth", "Sam"


# Data Types in Python
# 1) Strings
full_name = "John Doe"
secret_code = "Pa$$word"

# 2) Numbers (int, float, complex)
num_of_days = 365
pi_approx = 3.1416
complex_number = 1 + 5j

# 3) Boolean (bool)
is_learning = True
is_difficult = False

# Using Variables
print("My name is " + full_name)


# Terminal Outputs
# print() function
print("Hello World Again")
print("My name is " + full_name)

# To print integers, this is called TYPECASTING
print("My age is " + str(age))

# Typecasting - There may be times when you want to specify a type on to a variable. This can be done with casting. Here are some functions that can be used:
# 1) int() - convert value to int
# 2) float() - convert value to float
# 3) str() - convert value to string

# int() prints a whole number
print(int(3.5))

# float() puts a decimal point at the end of the value
print(float(10))


# F-Strings
print(f"Hi! My name is {full_name} and my age is {age}")
# F-Strings are like backticks (``) in JavaScript
# Available in Python versions 3.6 and above


# Operations in Python
print(1 + 10)  # Yu can directly add integers here
print(5 - 8)
print(18 * 9)
print(100 / 5)  # division but float output
print(100 // 5)  # division but integer output
print(2 ** 6)
print(18 % 4)


# Assignment Operators
num1 = 3
num1 += 4  # num1 + 4
# Assignment Operators include -=, *=, /=, %=

print(num1)


# Logical Operators
# AND = and
# OR = or
# NOT = not
print(True and False)
print(not False)
print(True or False)


# Comparison Operators
# Returns a boolean value
print(1 != 1)
print(1 == 1)
print(1 > 1)
print(1 < 1)
print(1 >= 1)
print(1 <= 1)
